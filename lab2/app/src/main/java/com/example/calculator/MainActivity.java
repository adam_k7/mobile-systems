package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.widget.SeekBar;
import android.widget.RatingBar;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    SeekBar seekBar;
    RatingBar starsBar;
    TextView value;
    TextView tip;
    TextView resultTextView;
    Button calculateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        starsBar = (RatingBar)findViewById(R.id.starsBar);
        seekBar = (SeekBar)findViewById(R.id.seekBar);
        value = findViewById(R.id.editValue);
        tip = findViewById(R.id.editTip);

        resultTextView = findViewById(R.id.calculated);
        calculateButton = findViewById(R.id.calculateButton);


        // CALCULATE BUTTON SETUP
        calculateButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onClick(View v) {
                float orderValue;
                float tipValue;
                if(value.getText().length() != 0) {
                    orderValue = Float.parseFloat(value.getText().toString());
                }
                else orderValue = 0;
                if(tip.getText().length() != 0) {
                    tipValue = Float.parseFloat(tip.getText().toString());
                }
                else tipValue = 0;
                float progress = (float)(seekBar.getProgress());
                float rating = (float)(starsBar.getRating());

                float result = calculate(orderValue,tipValue,progress,rating);
                resultTextView.setText(String.format("%.2f", result));
            }
        });
    }



    private float calculate(float orderValue, float tipValue, float progress, float rating) {
        float result = orderValue * tipValue/100 * progress/5 * rating/100;
        return result;
    }
}